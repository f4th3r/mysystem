THIS IS TEST postgresql cluster for k8s
#!!!!TESTED only in GCP k8s

**Schema of the example cluster**:

pgmaster (primary node1)  --|
|- pgslave1 (node2)       --|
|  |- pgslave2 (node3)    --|----pgpool (master_slave_mode stream)

The main idea to make High Availability pgsql cluster in k8s
**THIS cluster contains:**
-3 pgsql node with 1 master and to slave nodes
-repmgrd on all pgsql nodes (It allows to use automatic failover and check cluster status )
-2 replicas of  pgpool (balancing between nodes) 

**CONFIGURE**
This git repo has dirs like:
configs  namespace  nodes  pgpool  README.md  services  volumes

1. /config  contains two yaml files: config.yml  secret.yml
- config.yml - contains how many backends(plsql nodes) will be in your project - to add more backends config the 
app.db.pool.backends parametr.
- secret.yml contains all secret to conncet pgpool cluster, psql connection etc
2. /namespace - to create namespace in k8s
3.  /nodes  - pgsql nodes (to add mode just copy and configure one of yaml in this directory, DON'T forget to add backend in /config/config.yml
4. pgpool - contains config for pgpool.
- service-pgpool.yml creating service for pgpool !!!!!now it's type: LoadBalancer - it means k8s cluster give the external 
access from external users to pgpool.
- pgpool.yml - creating pgpool deployment
5. services - create services for pgsql nodes to communicate every nodes to each othets.
6. volumes - creating volumes for each nodes


**HOW TO START**
1. copy all files fro repo to your k8s cluster (git clone https://gitlab.com/f4th3r/mysystem.git)
2. start all yaml by:
Create namespace by kubectl create -f ./namespace/
Create configs: kubectl create -f ./configs/
Create volumes kubectl create -f ./volumes/
Create services kubectl create -f ./services/
Create nodes kubectl create -f ./nodes/
Create pgpool kubectl create -f ./pgpool/


**MONITORING**
# cluster status on nodes
kubectl -n mysystem get pod | grep db-node  | awk '{print $1}' | while read pod ; do echo "$pod": ; kubectl  -n mysystem exec $pod -- bash -c 'gosu postgres repmgr cluster show' ; done

# cluster status on pgpool
kubectl -n mysystem get pod | grep pgpool | awk '{print $1 }' | while read pod; do echo "$pod": ;kubectl -n mysystem exec $pod -- bash -c 'PGCONNECT_TIMEOUT=$CHECK_PGCONNECT_TIMEOUT  PGPASSWORD=$CHECK_PASSWORD psql -U $CHECK_USER -h 127.0.0.1 template1 -c "show pool_nodes"' ; done

**Health-checks**
To make sure you cluster works as expected without 'split-brain' or other issues, you have to setup health-checks and stop container if any health-check returns non-zero result. That is really useful when you use Kubernetes which has livenessProbe (check how to use it in the example)

Postgres containers:
/usr/local/bin/cluster/healthcheck/is_major_master.sh - detect if node acts as a 'false'-master and there is another master - with more standbys
Pgpool
/usr/local/bin/pgpool/has_enough_backends.sh [REQUIRED_NUM_OF_BACKENDS, default=$REQUIRE_MIN_BACKENDS] - check if there are enough backend behind pgpool
/usr/local/bin/pgpool/has_write_node.sh - check if one of the backend can be used as a master with write access

**Useful commands**
Get map of current cluster(on any postgres node):
gosu postgres repmgr cluster show - tries to connect to all nodes on request ignore status of node in $(get_repmgr_schema).$REPMGR_NODES_TABLE
gosu postgres psql $REPLICATION_DB -c "SELECT * FROM $(get_repmgr_schema).$REPMGR_NODES_TABLE" - just select data from tables
Get pgpool status (on any pgpool node): PGPASSWORD=$CHECK_PASSWORD psql -U $CHECK_USER -h localhost template1 -c "show pool_nodes"
In pgpool container check if primary node exists: /usr/local/bin/pgpool/has_write_node.sh
Any command might be wrapped with docker-compose or kubectl - docker-compose exec {NODE} bash -c '{COMMAND}' or kubectl exec {POD_NAME} -- bash -c '{COMMAND}'
